import React from "react";
import { Routes, Route } from "react-router-dom";
import App from "../App";
import Search from "../components/Search";
import Home from "../components/Home";

const Router = () => (
  <Routes>
    <Route path="/" element={<Home />} />
    <Route path="/search" element={<Search />} />
  </Routes>
);

export default Router;
