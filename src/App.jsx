import { ApolloClient, InMemoryCache, ApolloProvider } from "@apollo/client";
import Router from "../src/routes/Router.tsx";
import "./App.css";

function App() {
  const token = "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";

  const client = new ApolloClient({
    uri: "https://api.github.com/graphql",
    cache: new InMemoryCache(),
    headers: {
      authorization: `Bearer ${token}`,
    },
  });

  return (
    <ApolloProvider client={client}>
      <Router />
    </ApolloProvider>
  );
}

export default App;
