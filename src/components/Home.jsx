import { useQuery, gql } from "@apollo/client";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";

function Home() {
  return (
    <div>
      <h2>My first Apollo app 🚀</h2>
      <Link className="options__link" to="/search">
        <button> start </button>
      </Link>
    </div>
  );
}

export default Home;
