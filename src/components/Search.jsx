import { useQuery, gql } from "@apollo/client";
import { useEffect, useState } from "react";

function Search() {
  const [inputValue, setInputValue] = useState("");
  const GET_LOCATIONS = gql`
    query Bla($owner: String!) {
      user(login: $owner) {
        repositories(privacy: PUBLIC, ownerAffiliations: OWNER, first: 10) {
          edges {
            node {
              id
              name
              url
            }
          }
          nodes {
            contactLinks {
              name
              about
            }
          }
        }
      }
    }
  `;

  const { loading, error, data, refetch } = useQuery(GET_LOCATIONS, {
    variables: { owner: inputValue },
  });

  return (
    <div>
      <h2>My first Apollo app 🚀</h2>
      <p>write login</p>
      <input
        type="text"
        value={inputValue}
        onChange={(event) => setInputValue(event.target.value)}
      />

      {loading ? (
        <p>loading ... </p>
      ) : (
        data?.user.repositories.edges.map((el) => (
          <p>
            <a href={el.node.url}>{el.node.name}</a>
          </p>
        ))
      )}
    </div>
  );
}

export default Search;
